/*
  * Copyright (C) 2018 Michael Schmitz
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

//RCSwitchLibrary needs to be in a sibling folder
//WiringPi needs to be installed on the system
#include "../rc-switch/RCSwitch.h"
#include <cstdlib>
#include <cstdio>
#include <sys/stat.h>
#include <thread>
#include <map>
#include <cstring>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <unistd.h>
#include <iostream>

#define BUFLEN 512
#define PORT 5555
#define LENGTH 6
#define PIN 0

using namespace std;

void startTCPListener(RCSwitch rcSwitch);

map<string, bool> powerState = {{"1111110000", false},
                                {"1111101000", false},
                                {"1111100100", false},
                                {"1111100010", false},
                                {"1111111000", false},
                                {"1111100110", false}};

//PACKETS:
//0xCC 	0xAA powerOn
//		0xBB powerOff
//		0xCC toggle
//		0xDD queryState


int main(int argc, char *argv[]) {
    if (wiringPiSetup() == -1) return 1;

    RCSwitch rcSwitch = RCSwitch();
    rcSwitch.enableTransmit(PIN);

    rcSwitch.switchOff("11111", "10000");
    rcSwitch.switchOff("11111", "01000");
    rcSwitch.switchOff("11111", "00100");
    rcSwitch.switchOff("11111", "00010");
    rcSwitch.switchOff("11111", "11000");
    rcSwitch.switchOff("11111", "00110");

    thread tcpListener(startTCPListener, rcSwitch);
    tcpListener.join();

    return 0;
}

void startTCPListener(RCSwitch rcSwitch) {
    int handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    struct stat fileBuffer;
    struct sockaddr_in servAddr;
    int checksum0 = LENGTH - 2;
    int checksum1 = LENGTH - 1;
    
     int sockfd, newsockfd;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        perror("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(PORT);
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              perror("ERROR on binding");
    while (true) {
        //quits the thread if a file named "stop" is in the same directory
        if (stat("stop", &fileBuffer) == 0) exit(-1);
        struct sockaddr_in cliAddr;
        listen(sockfd,5);
        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
        if (newsockfd < 0) 
            perror("ERROR on accept");
        bzero(buffer,256);
        n = read(newsockfd,buffer,255);
        if (n < 0)
            perror("ERROR reading from socket");
        if (buffer[0] == 0xCC) {
            unsigned short sum = 0;
            for (int j = 0; j < checksum0; j++) {
                sum += buffer[j];
            }
            if ((((unsigned short) buffer[checksum0] << 8) | buffer[checksum1]) == sum) {
                char net[6];
                char device[6];
                for (int k = 0; k < 5; k++) {
                    net[k] = (buffer[2] >> k & 0x01) + 48;
                    device[k] = (buffer[3] >> k & 0x01) + 48;
                }
                net[5] = 0;
                device[5] = 0;
                string netDevice = net;
                netDevice += device;

                if (powerState.count(netDevice) != 0) {
                    if (buffer[1] == 0xAA) {
                        powerState.at(netDevice) = true;
                        rcSwitch.switchOn(net, device);
                    } else if (buffer[1] == 0xBB) {
                        powerState.at(netDevice) = false;
                        rcSwitch.switchOff(net, device);
                    } else if (buffer[1] == 0xCC) {
                        if (powerState.at(netDevice)) {
                            powerState.at(netDevice) = false;
                            rcSwitch.switchOff(net, device);
                        } else {
                            powerState.at(netDevice) = true;
                            rcSwitch.switchOn(net, device);
                        }
                    } else if (buffer[1] == 0xDD) {
                        //send tcp                            
                        int sumSend = 0;
                        unsigned char message[6];
                        message[0] = 0xDD;
                        message[1] = powerState[netDevice] ? '1' : '0';
                        message[2] = buffer[2];
                        message[3] = buffer[3];
                        for (unsigned char l : message) {
                            sum += l;
                        }
                        message[4] = static_cast<unsigned char>(sumSend >> 8);
                        message[5] = static_cast<unsigned char>(sumSend & 0x00FF);
                        n = write(newsockfd, message, 6);
                        if (n < 0) 
			    perror("ERROR writing to socket");                            
                    }
                }
            }
        }
        close(newsockfd);
    }
    close(sockfd);
}
