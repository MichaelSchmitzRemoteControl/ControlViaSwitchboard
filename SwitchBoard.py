#
#  Copyright (C) 2018 Michael Schmitz
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
#

import requests
import Adafruit_MPR121.MPR121 as MPR121

serveraddr = 'http://192.168.178.52:9080/mhz/toggle'
cap = MPR121.MPR121()
switch = {
    0: "10000",
    1: "01000",
    2: "00100",
    9: "00010",
    10: "00110",
    11: "11000"
}
if not cap.begin():
    print('Error initializing MPR121.')
    sys.exit(1)
last_touched = cap.touched()
while True:
    current_touched = cap.touched()
    for i in range(12):
        pin_bit = 1 << i
        if current_touched & pin_bit and not last_touched & pin_bit:
            touched = switch.get(i, "invalid")
            if touched != "invalid":
                requests.post(serveraddr, json={"net": "11111", "device": touched})
    last_touched = current_touched
