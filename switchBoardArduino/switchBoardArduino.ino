/*
  * Copyright (C) 2018 Michael Schmitz
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

#include <Wire.h>
#include "Adafruit_MPR121.h"

HTTPClient client;
Adafruit_MPR121 cap = Adafruit_MPR121();

const char* ssid ="SSID";
const char* pwd ="password";
uint16_t last_touched = 0;
uint16_t current_touched = 0;
const char* server = "http://serverip:port/mhz/toggle";

void setup() {
  //PIN0 Status LED indicates if setup runs completly
  //because of problems with Lolin Nodemcu ESP8266 WDT/ Soft WDT
  //in Wire.begin() (fails sometimes to initialize) 
  //-> solution: reboot
  pinMode(0, OUTPUT);
  digitalWrite(0, HIGH);
  Serial.begin(115200);
  
  Serial.println("MPR121 Capacitive Touch sensor"); 
  WiFi.begin(ssid, pwd);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("connected");
  
  // Default address is 0x5A, if tied to 3.3V its 0x5B
  // If tied to SDA its 0x5C and if SCL then 0x5D
  if (!cap.begin(0x5A)) {
    Serial.println("MPR121 not found");
    return;
  }
  Serial.println("MPR121 found!");
  digitalWrite(0, LOW);
  
}

void loop() {    
  String msg = "{\"net\": \"11111\", \"device\": \""; 
  current_touched = cap.touched();  
  for (int i=0; i<12; i++) {
    if ((current_touched & _BV(i)) && !(last_touched & _BV(i)) ) {
      switch (i) {
        case 6: {//left top
          msg += "00100\"}";
          break;
        }
        case 8: {//middle top
          msg += "01000\"}";
          break;
        }
        case 10: {//right top
          msg += "10000\"}";
          break;
        }
        case 7: {//left bottom
          msg += "00010\"}";
          break;
        }
        case 9: {//middle bottom
          msg += "00110\"}";
          break;
        }
        case 11: {//right bottom
          msg += "11000\"}";
          break;
        }
        default: {
          continue;
        }
      }
      
      client.begin(server);
      client.addHeader("Content-Type", "application/json");
      client.POST(msg);
      client.end();
    }
  }

  last_touched = current_touched;
}
